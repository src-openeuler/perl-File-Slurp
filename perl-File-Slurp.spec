Name: 		perl-File-Slurp
Summary:        Simple and Efficient Reading/Writing/Modifying of Complete File
Version: 	9999.32
Release: 	2
License: 	GPL-1.0-or-later OR Artistic-1.0-Perl
URL: 		https://metacpan.org/release/File-Slurp
Source0: 	https://cpan.metacpan.org/modules/by-module/File/File-Slurp-%{version}.tar.gz

BuildRequires:  perl-generators perl(ExtUtils::MakeMaker) perl(Carp) perl(Errno)
BuildRequires:  perl(Exporter) perl(Fcntl) perl(POSIX) perl(re) perl(strict)
BuildRequires:  perl(vars) perl(warnings) perl(File::Basename) perl(File::Spec) >= 3.01
BuildRequires:  perl(File::Temp) perl(IO::Handle) perl(lib) perl(overload) perl(Scalar::Util)
BuildRequires:  perl(Socket) perl(Symbol) perl(Test::More)

BuildArch: noarch

%{?perl_default_filter}

%description
This module provides subs that allow you to read or write entire files with
one simple call. They are designed to be simple to use, have flexible ways
to pass in or get the file contents and to be very efficient. There is also
a sub to read in all the files in a directory.

%package_help

%prep
%autosetup -p1 -n File-Slurp-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
%make_build

%install
make pure_install PERL_INSTALL_ROOT=%{buildroot}
chmod -R u+w %{buildroot}/*

%check
make test

%files
%{perl_vendorlib}/File

%files help
%doc Changes README.md
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 9999.32-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Aug 17 2020 lingsheng<lingsheng@huawei.com> - 9999.32-1
- Update to 9999.32

* Tue Nov 19 2019 caomeng<caomeng5@huawei.com> - 9999.19-22
- Package init
